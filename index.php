<?php
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
define('AppDir',__DIR__.'/app');
define('Local', (mb_strpos($_SERVER['HTTP_HOST'], '.loc')||(mb_substr($_SERVER['HTTP_HOST'], 0,7)=='192.168')||(in_array($_SERVER['HTTP_HOST'], ['127.0.0.1', 'localhost']))));
include AppDir . '/includes/init.php';

$router = new \s\Router();
$router->load($_GET['urls']);
$router->run();