class CarSelect2 {
    show_error(message, reason) {
        if (!this.wndError) {
            let html = `<div class="window mini" rel="UserSelect2_Error">
                    <div class="inner_content">
                        <h2>Ошибка</h2>
                        <div class="window_content _center"></div>
                    </div>
                </div>`;
            $('body').append(html);
            this.wndError = $('.window[rel="UserSelect2_Error"]');
            this.wndError.iwindow();
        }
        message = `<h3>${message}</h3>`;
        if (reason==='auth') {
            message += `<div class="form_button login_vk">Войти</div>`;
        } else {
            message += `<div class="form_button close_btn">Продолжить</div>`;
        }
        this.wndError.find('.window_content').html(message);
        this.wndError.iwindow('show');
    }
    formatCarRepo(repo) {
        if (repo.loading) return repo.text;
        return "<div class='select2-result-repository clearfix'>" +
            "<div class='select2-result-repository__meta'>" +
            "<div class='select2-result-repository__title'>" + repo.text + "</div>" +
            "<div class='select2-result-repository__description'>" + (repo.description ? repo.description : '') + "</div>" +
            "</div>" +
            "<div style='clear: both'></div>" +
            "</div>";
    }
    formatCarRepoSelection(data, container) {
        $(data.element).data('image', data.image);
        return data.text;
    }
    getUrlParameter(sParam) {
        var sPageURL = window.location.search.substring(1),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
            }
        }
    };
    constructor(els, callback=()=>{}, options={}) {
        if (els.length) {
            els.each((i, e)=>{
                let el = $(e);
                let types = {
                    visitor: {
                        url: '/ajax/visitors/select2',
                        placeholder: 'Посетитель'
                    },
                    user: {
                        url: '/ajax/users/select2',
                        placeholder: 'Пользователь'
                    },
                    car: {
                        url: '/ajax/cars/select2',
                        placeholder: 'Авто'
                    }
                };
                let type = 'car';
                if (el.data('type')) {
                    type = el.data('type');
                }
                let preload = el.data('preload');
                if (!preload) preload = '';
                let car = [];
                el.on('change', callback).select2({
                    ajax: {
                        url: types[type].url,
                        dataType: 'json',
                        delay: 250,
                        data:  (params) => {
                            return {
                                q: params.term,
                                page: params.page,
                                params: (el.data('params') ? el.data('params') : {}),
                                preload: preload
                            };
                        },
                        processResults: (data, params) => {
                            if (data.status==='error') {
                                this.show_error(data.message, data.reason);
                            }
                            car = data;
                            params.page = params.page || 1;
                            return {
                                results: data.items,
                                pagination: {
                                    more: (params.page * 30) < data.total_count
                                }
                            };
                        },
                        cache: true
                    },
                    placeholder: (el.data('placeholder') ? el.data('placeholder') : types[type].placeholder),
                    allowClear: true,
                    language: 'ru',
                    escapeMarkup: (markup) => {
                        return markup;
                    },
                    //minimumInputLength: 1,
                    templateResult: this.formatCarRepo,
                    templateSelection: this.formatCarRepoSelection
                });
                if (this.getUrlParameter('id') != undefined) {
                    el.select2('trigger', 'select', {data: {id: $('#car_brand').attr('data-car_brand_id'), text: $('#car_brand').attr('data-car_brand')}});
                }
            });

        }
    }
}