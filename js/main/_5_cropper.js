class CropperWindow {
    constructor(el, callback=()=>{}) {
        this.el = el;
        this.el.find('.controls .item').on('click', (event)=> {
            let cropper = this.el.find('.cropper_image img').data('cropper');
            switch($(event.currentTarget).attr('rel')) {
                case 'rotate':
                    cropper.rotate(($(event.currentTarget).data('type') === 'left' ? 90 : -90));
                    break;
                case 'loop':
                    cropper.zoom($(event.currentTarget).data('type') === 'plus' ? 0.1 : -0.1);
                    break;
                case 'exchange':
                    cropper.scaleX(-cropper.imageData.scaleX);
                    break;
            }
        });
        this.el.find('.actions .item').on('click', (e)=> {
            if (!$(e.currentTarget).hasClass('load')) {
                $(e.currentTarget).formbutton('start');
                let cropper = this.el.find('.cropper_image img').data('cropper');
                switch ($(e.currentTarget).attr('rel')) {
                    case 'cancel':
                        cropper.destroy();
                        this.el.closest('.window').iwindow('hide');
                        $(e.currentTarget).formbutton('stop');
                        break;
                    case 'save':
                        callback(cropper);
                        break;
                }
            }
        });
    }
}
class CropperImage {
    show_error(error) {
        alert(error);
    }
    open(e){
        let form_data = new FormData;
        form_data.append('img', this.file.prop('files')[0]);
        this.el.find('.icon_download').addClass('load');
        $.ajax({
            url: '/ajax/main/image2tmp',
            dataType: 'text',
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post',
            success: (r) => {
                let a = $.parseJSON(r);
                this.el.find('.icon_download input').val('');
                if (a['status'] === 'ok') {
                    this.full_image.val(a['filename']);
                    this.wndCropp.iwindow('show');
                    let aspectRatio = this.el.data('aspectratio');
                    if (!aspectRatio) aspectRatio = 1;
                    if (!this.main_photo_crop.find('img').data('cropper')) {
                        this.main_photo_crop.html(`<img src="${a['url']}"></div>`);
                        this.main_photo_crop.find('img').cropper({
                            aspectRatio: aspectRatio,
                            dragMode: 'move',
                            cropBoxMovable: false,
                            cropBoxResizable: false,
                            viewMode:1,
                            autoCropArea:0.9,
                            //minCanvasWidth: (this.el.data('width') ? this.el.data('width') : null),
                            //minCanvasHeight: (this.el.data('height') ? this.el.data('height') : null),
                        });

                        let Cropper_Window = new CropperWindow(this.wndCropp.find('.cropper_window'), (cropper)=>{
                            $.post('/ajax/main/crop_image', {
                                imgUrl: cropper.now_url,
                                data: jQuery.toJSON(cropper.getData())
                            }, (r)=>{
                                let a = jQuery.parseJSON(r);
                                this.wndCropp.find('.actions .item.load').formbutton('stop');
                                if (a.status==='ok') {
                                    this.crop_image.val(a['filename']);
                                    this.el.addClass('loaded');
                                    this.el.css({'background-image': 'url("'+a['url']+'")'});
                                    this.wndCropp.iwindow('hide');
                                    this.callback(this.crop_image.val(), this.full_image.val());
                                } else {
                                    this.show_error(a.message);
                                }
                            });
                        });
                    } else {
                        this.main_photo_crop.find('img').data('cropper').replace(a['url']);
                    }
                    this.main_photo_crop.find('img').data('cropper').now_url = a['url'];
                } else {
                    this.show_error(a.message);
                }
                this.el.find('.icon_download').removeClass('load');
            },
            error: (jqXHR, exception) => {
                this.file.val('');
                this.show_error(jqXHR.responseText);
                this.el.find('.icon_download').removeClass('load');
            }
        });
    }

    constructor(el, callback=()=>{}) {
        if (el.length) {
            this.el = el;
            this.callback = callback;

            if ((this.el.data('image'))&&(this.el.data('image')!=='')) {
                this.el.css('background-image', 'url("'+this.el.data('image')+'")');
                this.el.addClass('loaded');
            }

            this.wndCropp = $('.window[rel="CropperWindow_'+this.el.data('name')+'"]');
            if (this.wndCropp.length===0) {
                let html = `<div class="window" rel="CropperWindow_${this.el.data('name')}">
                    <div class="inner_content">
                        <div class="close"></div>
                        <h2>Обрежьте фотографию</h2>
                        <div class="window_content cropper_window">
                            <div class="controls">
                                <div class="item" rel="rotate" data-type="left"><div class="icon-undo"></div></div>
                                <div class="item" rel="rotate" data-type="right"><div class="icon-redo"></div></div>
                                <div class="item" rel="loop" data-type="plus"><i class="fa fa-search-plus" aria-hidden="true"></i></div>
                                <div class="item" rel="loop" data-type="minus"><i class="fa fa-search-minus" aria-hidden="true"></i></div>
                                <div class="item" rel="exchange"><i class="fa fa-exchange" aria-hidden="true"></i></div>
                            </div>
                            <div class="cropper_image"></div>
                            <div class="actions">
                                <div class="form_button item" rel="cancel">Отменить</div>
                                <div class="form_button item" rel="save">Сохранить</div>
                            </div>
                        </div>
                    </div>
                </div>`;
                $('body').append(html);
                this.wndCropp = $('.window[rel="CropperWindow_'+this.el.data('name')+'"]');
                this.wndCropp.iwindow();
                this.wndCropp.find('.form_button').formbutton();
            }

            this.main_photo_crop = this.wndCropp.find('.cropper_image');
            this.el.html(`<input type="hidden" class="crop_full_image" name="${this.el.data('name')}[full]">
                <input type="hidden" class="crop_crop_image" name="${this.el.data('name')}[crop]">
                <div class="fogging">
                    <div class="icon_download"><input type="file"/></div>
                </div>`);

            this.file = this.el.find('.icon_download input');
            this.full_image = this.el.find('.crop_full_image');
            this.crop_image = this.el.find('.crop_crop_image');

            this.file.on('change', ()=>{
                this.open();
            });
        }
    }
}