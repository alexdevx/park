class StaffAdmin_Edit {
    save() {
        if (!this.btn_save.hasClass('load')) {
            this.btn_save.formbutton('start');
            this.error.html('');
            $.post('/ajax/staff/save', this.el.find('input, select, textarea').serializeArray(), (r)=>{
                let a = jQuery.parseJSON(r);
                if (a.status==='ok') {
                    location.href = '?';
                } else {
                    this.btn_save.formbutton('stop');
                    this.error.html(a.message);
                }
            });
        }
    }
    del() {
        if ((!this.btn_del.hasClass('load'))&&(confirm('Удалить пользователя?'))) {
            this.btn_del.formbutton('start');
            this.error.html('');
            $.post('/ajax/staff/del', {
                user_id: this.el.find('input[name="user_id"]').val()
            }, (r)=>{
                let a = jQuery.parseJSON(r);
                if (a.status==='ok') {
                    location.href = '?';
                } else {
                    this.btn_del.formbutton('stop');
                    this.error.html(a.message);
                }
            });
        }
    }
    constructor(el) {
        if (el.length) {
            this.el = el;
            this.error = this.el.find('.error');
            this.btn_save = this.el.find('.form_button.save');
            this.btn_del = this.el.find('.form_button.delete');
            let _Image4Crop = new CropperImage(this.el.find('.image4crop'));
            let Select2 = new UserSelect2(this.el.find('select[name="user2user[]"]'));
            this.btn_save.on('click', ()=>{
                this.save();
            });
            this.btn_del.on('click', ()=>{
                this.del();
            });
            this.el.find('select[name="type"]').on('change',(e)=>{
                if (parseInt($(e.currentTarget).val())===0) {
                    this.el.find('.user2user').show();
                } else {
                    this.el.find('.user2user').hide();
                }
            }).change();
        }
    }
}
class StaffAdmin {
    load_items() {
        if (!this.loading) {
            this.loading = true;
            this.iload.show();
            $.post('/ajax/staff/get_items', {
                find: this.find_data,
                i: this.i
            }, (r) => {
                let a = jQuery.parseJSON(r);
                if (a['status'] === 'ok') {
                    this.items.append(a['html']);
                    let non_rel = this.items.find('.item[rel="0"]');
                    if (non_rel.length>0) {
                        non_rel.ripple();
                        non_rel.attr('rel', 1);
                    }
                    this.el.trigger('resize');
                    this.i = a['i'];
                    this.hasItems = (this.i < a['count']);
                    this.loading = false;
                    this.iload.hide();
                    if ((this.hasItems) && (parseInt(this.el.height()) < parseInt($(window).height()) + 200)) {
                        this.load_items();
                    }
                } else {
                    alert(a.message);
                }
            });
        }
    }

    get_finder_params() {
        this.find_data.text = this.finder.find('input.text').val();
    }

    find() {
        this.get_finder_params();
        this.i = 0;
        this.hasItems = true;
        this.loading = false;
        this.items.html('');
        this.load_items();
    }

    constructor(el) {
        if (el.length) {
            this.el = el;
            this.items = this.el.find('.items');
            this.iload = this.el.find('.iload');
            this.finder = this.el.find('.items_finder');
            this.find_data = {};
            this.btn_add = this.el.find('.btn.add_staff');
            this.wndDelete = $('.window[rel="DeleteStaff"]');
            this.user_id = 0;

            this.btn_add.on('click', ()=>{
                this.add();
            });

            this.finder.find('input.text').on('keyup', (e)=>{
                if (e.keyCode===13) {
                    this.find();
                }
            });
            this.finder.find('.find_btn').on('click', ()=>{
                this.find();
            });
            let Select2 = new UserSelect2(this.el.find('select[name="staff_id"]'));

            this.find();
            $(window).scroll(() => {
                if ((this.hasItems) && (parseInt($(window).scrollTop()) + parseInt($(window).height()) > parseInt($(document).height()) - 100)) {
                    this.load_items();
                }
            });
        }
    }
}