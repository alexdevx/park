class ForgotForm {
    constructor(el) {
        if (el.length) {
            this.el = el;
            this.el_error = this.el.find('.error[rel="send_restore"]');
            this.el_btn = this.el.find('.send_restore');

            this.confirm = {};
            this.confirm.el = this.el.find('.step_forgot_2');
            this.confirm.el_error = this.confirm.el.find('.error[rel="confirm_restore"]');
            this.confirm.el_btn = this.confirm.el.find('.confirm_restore');

            this.el_btn.click(() => {
                this.el_btn.formbutton('start');
                this.el_error.hide().html('');
                $.post('/ajax/users/restore_send', {
                    email: this.el.find('input[name="email"]').val()
                }, (r) => {
                    let a = jQuery.parseJSON(r);
                    this.el_btn.formbutton('stop');
                    if (a.status === 'ok') {
                        this.confirm.el.show();
                        this.confirm.el.find('input[name="code"]').focus();
                    } else {
                        this.el_error.show().html(a['message']);
                    }
                });
            });
            this.el.find('input[name="email"]').keyup((e) => {
                if(e.keyCode === 13){
                    this.el_btn.click();
                }
            });
            this.confirm.el_btn.click(() => {
                this.confirm.el_btn.formbutton('start');
                this.confirm.el_error.hide().html('');
                $.post('/ajax/users/restore_confirm', {
                    email: this.el.find('input[name="email"]').val(),
                    code: this.el.find('input[name="code"]').val(),
                    pass: this.el.find('input[name="pass"]').val(),
                    pass1: this.el.find('input[name="pass1"]').val()
                }, (r) => {
                    let a = jQuery.parseJSON(r);
                    if (a.status === 'ok') {
                        location.href='/';
                    } else {
                        this.confirm.el_btn.formbutton('stop');
                        this.confirm.el_error.show().html(a['message']);
                    }
                });
            });
            this.confirm.el.find('input[name="code"]').keyup((e) => {
                if(e.keyCode === 13){
                    this.confirm.el_btn.click();
                }
            });
        }
    }
}
