class Shield {
    load_items() {
        this.iload.show();
        $.post('/ajax/shield/get_items', {
            find: this.find_data,
            i: this.i
        }, (r) => {
            let a = jQuery.parseJSON(r);
            if (a['status'] === 'ok') {
                if (parseInt(a['start'])===0) {
                    console.log('!');
                    this.items.html('');
                }
                this.items.append(a['html']);
                this.el.trigger('resize');
                this.i = a['i'];
                this.hasItems = (this.i < a['count']);
                this.iload.hide();
                if ((this.hasItems) && (parseInt(this.el.height()) < parseInt($(window).height()) + 200)) {
                    this.load_items();
                }
            }
        });
    }
    reset_find() {
        this.finder.find('input.text').val('');
    }
    get_finder_params() {
        this.find_data.text = this.finder.find('input.text').val();
        this.find_data.company_user_id = this.finder.find('select[name="company_user_id"]').val();
        this.find_data.status = this.finder.find('.item_finder_tabs .tab.active').attr('rel');
        this.find_data.type = this.finder.find('.item_finder_types .tab.active').attr('rel');
    }

    find() {
        this.get_finder_params();
        this.i = 0;
        this.hasItems = true;
        this.items.html('');
        this.load_items();
    }

    // start activity in promise. set id to value for 'in process' state
    func() {
        this.timeoutID = -1;
        var that = this;
        this.promise = new Promise (function (resolve) {
            that.find(); // execute main activity
            setTimeout(function() {
                that.timeoutID = undefined;
            }, 0);
            resolve(1);
        });
    }
    // function check activity state and start activity timer (not set yet) or refresh (setuped but not started yet) timer or create then for promise (in process state)
    setup() {
        var that = this;
        if (typeof this.timeoutID === "number") {
            if (this.timeoutID !== -1) { // refresh
                this.cancel();
            } else { // then for promise
                this.promise.then(function(val) {
                    window.setTimeout(function() {
                        that.setup();
                    },0);
                });
            }
        }
        // setup timer
        if (this.timeoutID === undefined) {
            this.timeoutID = window.setTimeout(function() {
                that.func();
            }, 1000);
        }
    }
    // clear timer
    cancel() {
        window.clearTimeout(this.timeoutID);
        this.timeoutID = undefined;
    }

    constructor(el) {
        if (el.length) {

            this.el = el;
            this.items = this.el.find('.items');
            this.iload = this.el.find('.iload');
            this.finder = this.el.find('.items_finder');
            this.find_data = {};

            this.finder.find('input.text').on('keyup', (e)=>{
                this.setup();
            });
            this.finder.find('select[name="status"]').on('change', (e)=>{
                this.find();
            });
            this.finder.find('.find_btn').on('click', ()=>{
                this.find();
            });
            let Select2 = new UserSelect2(this.finder.find('select[name="company_user_id"]'), ()=>{
                this.find();
            });

            this.finder.find('.item_finder_tabs .tab').on('click', (e)=>{
                this.finder.find('.item_finder_tabs .tab.active').removeClass('active');
                $(e.currentTarget).addClass('active');
                this.find();
            });

            this.finder.find('.item_finder_types .tab').on('click', (e)=>{
                this.finder.find('.item_finder_types .tab.active').removeClass('active');
                $(e.currentTarget).addClass('active');
                this.find();
            });

            this.find();
            $(window).scroll(() => {
                if ((this.hasItems) && (parseInt($(window).scrollTop()) + parseInt($(window).height()) > parseInt($(document).height()) - 100)) {
                    this.load_items();
                }
            });
            this.el.on('click', '.items .item .commands .btn', (e)=>{
                let btn = $(e.currentTarget);
                let item = btn.closest('.item');
                if ((!btn.hasClass('load'))&&(item.data('status')!=='out')) {
                    btn.addClass('load');
                    if (item.find('.weight_id').val() == undefined) {
                        $.post('/ajax/shield/change', {
                            visitor_id: item.data('id'),
                            status: item.attr('rel')
                        }, (r)=>{
                            btn.removeClass('load');
                            let a = jQuery.parseJSON(r);
                            if (a.status === 'ok') {
                                if (a.btn_text==='') {
                                    btn.hide();
                                } else {
                                    btn.html(a.btn_text);
                                }
                                item.attr('rel', a.status_name);
                                btn.closest('.item').find('.status').hide().html(a.status_text).show(300);
                                this.reset_find();
                            } else {
                                alert(a.message);
                            }
                        });
                    } else {
                        $.post('/ajax/shield/change', {
                            visitor_id: item.data('id'),
                            status: item.attr('rel'),
                            car_number: item.find('.car_number').val(),
                            weight_id: item.find('.weight_id').val()
                        }, (r)=>{
                            btn.removeClass('load');
                            let a = jQuery.parseJSON(r);
                            if (a.status === 'ok') {
                                if (a.btn_text==='') {
                                    btn.hide();
                                } else {
                                    btn.html(a.btn_text);
                                }
                                item.attr('rel', a.status_name);
                                btn.closest('.item').find('.status').hide().html(a.status_text).show(300);
                                this.reset_find();
                            } else {
                                alert(a.message);
                            }
                        });
                    }
                }
            })
        }
    }
}