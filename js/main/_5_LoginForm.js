class LoginForm {
    constructor(el) {
        if (el.length) {
            this.el = el;
            this.el_form = this.el.find('form');
            this.el_error = this.el.find('.error[rel="main"]');
            this.el_btn = this.el.find('.send_login');

            this.el_form.ajaxForm({
                dataType: null,
                beforeSubmit: () => {
                    if (!this.el_btn.hasClass('load')) {
                        this.el_error.hide();
                        this.el_btn.formbutton('start');
                        return true;
                    } else {
                        return false;
                    }
                },
                success: (r) => {
                    let a = jQuery.parseJSON(r);
                    if (a.status === 'ok') {
                        location.href = '';
                    } else {
                        this.el_btn.formbutton('stop');
                        this.el_error.show().html(a['message']);
                        
                    }
                },
                complete: () => {

                }
            });
            this.el_btn.on('click', () => {
                
                this.el_form.submit();
            });
            this.el.find('.inputs_login input').keyup((e) => {
                if (e.keyCode === 13) {
                    this.el_form.submit();
                }
            });
            this.el.find('.soc_icons .item.vk').click(() => {
                this.type_confirm='vk';
                this.el.find('.loader_login_content').show();
                this.el.find('.content_loader_hide').hide();
                VK.Auth.login((response) => {
                    if (response.session) {
                        $.post('/ajax/users/auth_vk', {

                        }, (r) => {
                            let a = jQuery.parseJSON(r);
                            if (a.status === 'ok') {
                                if (a.type === 0) {
                                    location.href='';
                                } else {
                                    this.el.find('.inputs_login').hide();
                                    this.el.find('.inputs_confirm').show();
                                    this.el.find('.confirm_email').focus();
                                    this.el.find('.loader_login_content').hide();
                                    this.el.find('.content_loader_hide').show();
                                }
                            } else {
                                this.el.find('.error[rel="soc"]').html(a['message']).show();
                                this.el.find('.loader_login_content').hide();
                                this.el.find('.content_loader_hide').show();
                            }

                        });
                    } else {
                        this.el.find('.error[rel="soc"]').html('Нажмите разрешить в открывшемся окне').show();
                    }
                });
            });
            this.el.find('.soc_icons .item.fb').click(() => {
                this.type_confirm='fb';
                this.el.find('.loader_login_content').show();
                this.el.find('.content_loader_hide').hide();
                FB.login((response) => {
                    $.post('/ajax/users/auth_fb', {

                    }, (r) => {
                        let a = jQuery.parseJSON(r);
                        if (a.status === 'ok') {
                            this.fb_token = a['token'];
                            if (a.type === 0) {
                                location.href='';
                            } else {
                                this.el.find('.confirm_email').val(a['email']);

                                this.el.find('.inputs_login').hide();
                                this.el.find('.inputs_confirm').show();

                                this.el.find('.confirm_email').focus();
                            }
                        } else {
                            this.el.find('.error[rel="soc"]').html(a['message']).show();
                        }
                        this.el.find('.loader_login_content').hide();
                        this.el.find('.content_loader_hide').show();
                    });
                }, {scope: 'public_profile,email'});
            });
            this.el.find('.confirm_send').click(() => {
                $.post('/ajax/users/confirm_soc_email', {
                    email: this.el.find('.confirm_email').val(),
                    hash:  this.el.find('.confirm_hash').val()
                }, (r) => {
                    let a = jQuery.parseJSON(r);
                    if (a.status === 'ok') {
                        this.el.find('.confirm_hash').val(a['hash']);
                        this.el.find('.step_confirm_2').show();
                        this.el.find('.confirm_code').focus();
                    } else {
                        this.el.find('.error[rel="confirm_send"]').html(a['message']).show();
                    }
                });
            });
            this.el.find('.confirm_email').keyup((e) => {
                if (e.keyCode===13) {
                    this.el.find('.confirm_send').click();
                }
            });
            this.el.find('.confirm_login').click(() => {
                this.el.find('.content_loader_hide').hide();
                this.el.find('.loader_login_content').show();
                $.post('/ajax/users/confirm_soc', {
                    email: this.el.find('.confirm_email').val(),
                    code: this.el.find('.confirm_code').val(),
                    type: this.type_confirm,
                    fb_token: this.fb_token,
                    hash:  this.el.find('.confirm_hash').val()
                }, (r) => {
                    let a = jQuery.parseJSON(r);
                    if (a.status ==='ok') {
                        location.href='';
                    } else {
                        this.el.find('.loader_login_content').hide();
                        this.el.find('.content_loader_hide').show();
                        this.el.find('.error[rel="confirm_login"]').html(a['message']).show();
                    }
                });
            });
            this.el.find('.confirm_code').keyup((e) => {
                if (e.keyCode===13) {
                    this.el.find('.confirm_login').click();
                }
            });
        }
    }
}