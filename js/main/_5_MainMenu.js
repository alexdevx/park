class MainMenu {
    toogle() {
        if (this.icon_show.hasClass('active')) {
            this.el.removeClass('show');
            this.icon_show.removeClass('active');
            this.overflow.hide();
        } else {
            this.el.addClass('show');
            this.icon_show.addClass('active');
            this.overflow.show();
        }
    }
    constructor(el) {
        this.el = el;
        this.show_btn = $('header .show_menu');
        this.icon_show = this.show_btn.find('.nav-icon');
        this.overflow = $('.overflow_menu');
        this.show_btn.on('click', ()=>{
            this.toogle();
        });
        this.overflow.on('click', ()=>{
            this.toogle();
        });
    }
}