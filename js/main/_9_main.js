let _MainMenu, _LoginForm, _RegisterForm, _ForgotForm, _UserProfile,
    _UsersAdmin, _UsersAdmin_Edit,
    _VisitorsAdmin, _VisitorsAdmin_Edit, _CarVisitorsAdmin_Edit, _CarVisitorsAdmin,
    _Shield,
    _StaffAdmin, _StaffAdmin_Edit;
$(function(){
    let btn = $('.btn, .form_button');
    btn.formbutton();
    $('.btn, .form_button, .ripple').ripple();

    $('.window').iwindow();
    $('.linkimg').fancybox({
        animationEffect : "zoom",
        idleTime: 30
    });

    $(document).on('click', 'form .submit', function(){
        $(this).closest('form').submit();
    });

    $('.datepicker').datepicker({changeYear: true, changeMonth: true, regional: 'ru', dateFormat: 'dd.mm.yy'});

    _MainMenu = new MainMenu($('.main_menu'));
    _LoginForm = new LoginForm($('.user_login'));
    _RegisterForm = new RegisterForm($('.user_reg'));
    _ForgotForm = new ForgotForm($('.user_forgot'));
    _UserProfile = new UserProfile($('.UserProfile'));
    _UsersAdmin = new UsersAdmin($('.UsersAdmin'));
    _UsersAdmin_Edit = new UsersAdmin_Edit($('.UsersAdmin_Edit'));
    _VisitorsAdmin = new VisitorsAdmin($('.VisitorsAdmin'));
    _VisitorsAdmin_Edit = new VisitorsAdmin_Edit($('.VisitorsAdmin_Edit'));
    _CarVisitorsAdmin_Edit = new CarVisitorsAdmin_Edit($('.CarVisitorsAdmin_Edit'));
    _CarVisitorsAdmin = new CarVisitorsAdmin($('.CarVisitorsAdmin'));
    _Shield = new Shield($('.Shield'));
    _StaffAdmin = new StaffAdmin($('.StaffAdmin'));
    _StaffAdmin_Edit = new StaffAdmin_Edit($('.StaffAdmin_Edit'));
});