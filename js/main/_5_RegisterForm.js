class RegisterForm {
    constructor(el) {
        if (el.length) {
            this.el = el;
            this.el_btn = this.el.find('.send_reg');
            this.el_error = this.el.find('.error[rel="send"]');

            this.step2 = {};
            this.step2.el = this.el.find('.step_reg_2');
            this.step2.el_btn = this.step2.el.find('.confirm_reg');
            this.step2.el_error = this.step2.el.find('.error[rel="reg"]');

            this.el_btn.click(() => {
                if (!this.el_btn.hasClass('load')) {
                    this.el_btn.formbutton('start');
                    this.el_error.hide().html('');
                    $.post('/ajax/users/reg_send', {
                        email: this.el.find('input[name="email"]').val(),
                        pass: this.el.find('input[name="pass"]').val(),
                        pass1: this.el.find('input[name="pass1"]').val(),
                        name: this.el.find('input[name="name"]').val(),
                        hash: this.el.find('input[name="hash"]').val()
                    }, (r) => {
                        let a = jQuery.parseJSON(r);
                        this.el_btn.formbutton('stop');
                        if (a.status === 'ok') {
                            this.el.html('<div class="title1">Вы успешно зарегистрировались</div>' +
                                '<a href="/" class="btn block">Продолжить</a>');
                            /*
                            this.el.find('input[name="hash"]').val(a['hash']);
                            this.step2.el.show();
                            this.step2.el.find('input[name="code"]').focus();*/
                        } else {
                            this.el_error.show().html(a['message']);
                        }
                    });
                }
            });
            this.step2.el_btn.click((e) => {
                if (!this.step2.el_btn.hasClass('load')) {
                    this.step2.el_btn.formbutton('start');
                    this.step2.el_error.hide().html('');
                    $.post('/ajax/users/reg_confirm', {
                        email: this.el.find('input[name="email"]').val(),
                        pass: this.el.find('input[name="pass"]').val(),
                        pass1: this.el.find('input[name="pass1"]').val(),
                        name: this.el.find('input[name="name"]').val(),
                        code: this.el.find('input[name="code"]').val(),
                        hash: this.el.find('input[name="hash"]').val()
                    }, (r) => {
                        let a = jQuery.parseJSON(r);
                        this.step2.el_btn.formbutton('stop');
                        if (a.status === 'ok') {
                            this.el.html('<div class="title1">Вы успешно зарегистрировались</div>' +
                                '<a href="/" class="btn block">Продолжить</a>');
                        } else {
                            this.step2.el_error.show().html(a['message']);
                        }
                    });
                }
            });
        }
    }
}