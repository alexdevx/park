<?php
namespace s;

class Forms {
    /**
     * Загрузить картинки во временную папку
     *
     * @param string $name - Имя массива FILES
     * @param array $image_config - Конфигурация загрузки картинок
     * @return array - Массив с именами сохраненных картинок
     */
    public static function LoadImage2Tmp($file, $image_config=Array())
    {
        $arr = Array('status' => 'ok');
        if ((isset($file['tmp_name']))&&($file['tmp_name'] != '')) {
            $arr = self::CheckFile($file['tmp_name'], ['img']);
            if ($arr['status']=='ok') {
                $tmp_dir = $_SERVER['DOCUMENT_ROOT'] . '/tmp/';
                $filename = Forms::GetFilename($tmp_dir, time().'.'.$arr['ext']);

                if (move_uploaded_file($file['tmp_name'], $tmp_dir . $filename)) {
                    if (count($image_config)) {
                        $image_class = new \s\SimpleImage();
                        $image_class->load($tmp_dir . $filename);

                        if ($image_config[0]['crop']) {
                            $scale_h = $image_config[0]['h'] / $image_class->getHeight() * 100;
                            $scale_w = $image_config[0]['w'] / $image_class->getWidth() * 100;

                            if ($scale_h > $scale_w) {
                                $image_class->scale($scale_h);
                            } else {
                                $image_class->scale($scale_w);
                            }

                            $image_class->crop(
                                round($image_class->getWidth() / 2 - $image_config[0]['w'] / 2),
                                round($image_class->getHeight() / 2 - $image_config[0]['h'] / 2),
                                $image_config[0]['w'],
                                $image_config[0]['h']);
                        } elseif ($image_config[0]['w']) {
                            $image_class->resizeToWidth($image_config[0]['w']);
                            $image_class->resizeToHeight($image_config[0]['h']);
                        }
                        $image_class->save($tmp_dir . $filename);
                    }
                    $arr['url'] = '/tmp/' . $filename;
                    $arr['filename'] = $filename;
                } else {
                    $arr['status'] = 'error';
                    $arr['message'] = 'Файл не загружен';
                }
            }
        } else {
            $arr['status'] = 'error';
            $arr['message'] = 'Файл не загружен. Возможно его размер больше ' . ini_get('upload_max_filesize');
        }
        return $arr;
    }

    public static function LoadImage($file, $image_config=[], $rotate=0)
    {
        $arr = Array('status' => 'ok');
        if ((isset($file['tmp_name']))&&($file['tmp_name'] != '')) {
            $arr = self::CheckFile($file['tmp_name'], ['img']);
            if ($arr['status']=='ok') {

                $dir = $image_config[0]['dir'];
                $filename = Forms::GetFilename($dir, time().'.'.$arr['ext']);

                $tmp_dir = $_SERVER['DOCUMENT_ROOT'] . '/tmp/';
                $filename_tmp = Forms::GetFilename($tmp_dir, time().'.'.$arr['ext']);

                if (move_uploaded_file($file['tmp_name'], $tmp_dir . $filename_tmp)) {
                    self::SaveImage($tmp_dir . $filename_tmp, $filename, $image_config, $rotate);
                    unlink($tmp_dir . $filename_tmp);
                    $arr['url'] = $image_config[0]['url'] . $filename;
                    $arr['filename'] = $filename;
                } else {
                    $arr['status'] = 'error';
                    $arr['message'] = 'Файл не загружен';
                }
            }
        } else {
            $arr['status'] = 'error';
            $arr['message'] = 'Файл не загружен. Возможно его размер больше ' . ini_get('upload_max_filesize');
        }
        return $arr;
    }

    public static function LoadFromTmp($filename, $image_config, $old='', $unlink = true)
    {
        $arr = ['status' => 'ok'];
        $name = preg_replace("/\(^[0-9a-zA-Z_]+\)/", "", $filename);
        $tmp_dir = $_SERVER['DOCUMENT_ROOT'] . '/tmp/';
        $src_filename = $tmp_dir . $name;

        $arr = self::CheckFile($src_filename, ['img']);
        if ($arr['status']=='ok') {
            $dst_filename = Forms::GetFilename($image_config[0]['dir'], time().'.'.$arr['ext']);
            $arr['filename'] = $dst_filename;
            self::SaveImage($src_filename, $dst_filename, $image_config);

            if ($old) {
                foreach ($image_config as $index1 => $value) {
                    if (file_exists($value['dir'] . $old))
                        unlink($value['dir'] . $old);
                }
            }
            if ($unlink)
                unlink($src_filename);
        }
        return $arr;
    }

    public static function cropTmp($name, $params) {
        $arr = ['status'=>'ok'];
        $name =  preg_replace("/\(^[0-9a-zA-Z_]+\)/", "", explode('/', $name)[2]);
        $tmp_dir = $_SERVER['DOCUMENT_ROOT'] . '/tmp/';
        $src_filename = $tmp_dir.$name;

        if (!file_exists($src_filename)) {
            $arr['status']='error';
            $arr['message']='Файл не найден';
        }
        if ($arr['status']=='ok') {
            $arr = self::CheckFile($src_filename, ['img']);
        }
        if ($arr['status']=='ok') {
            $output_filename = self::GetFilename($tmp_dir, time().'.'.$arr['ext']);
            if (!is_writable(dirname($tmp_dir . $output_filename))) {
                $arr = Array(
                    "status" => 'error',
                    "message" => 'Ошибка загрузки файла'
                );
            }
        }
        if ($arr['status']=='ok') {
            $cropper = new \s\Cropper($src_filename, $params, $tmp_dir .$output_filename);
            if (!$cropper->getMsg()) {
                $arr['url'] = '/tmp/' . $output_filename;
                $arr['filename'] = $output_filename;
            } else {
                $arr['status'] = 'error';
                $arr['message'] = $cropper->getMsg();
            }
        }
        return $arr;
    }

    public static function CheckFile($filename, $types, $ext='') {
        $arr = ['status'=>'ok'];
        $mimes = [
            'zip'=>[
                'title'=>'ZIP',
                'items'=>[
                    'application/x-compressed'=>'zip',
                    'application/x-zip-compressed'=>'zip',
                    'application/zip'=>'zip',
                    'multipart/x-zip'=>'zip'
                ]
            ],
            'pdf'=>[
                'title'=>'PDF',
                'items'=>[
                    'application/pdf'=>'pdf'
                ]
            ],
            'doc'=>[
                'title'=>'xls, xlsx',
                'items'=>[
                    'application/vnd.ms-excel'=>'xls',
                    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'=>'xlsx',
                ]
            ],
            'img'=>[
                'title'=>'PNG, JPG, GIF',
                'items'=>[
                    'image/png'=>'png',
                    'image/jpeg'=>'jpg',
                    'image/gif'=>'gif'
                ]
            ],
            'txt'=>[
                'title'=>'TXT',
                'items'=>[
                    'text/plain'=>'txt'
                ]
            ]
        ];
        if (file_exists($filename)) {
            $finfo = finfo_open(FILEINFO_MIME_TYPE);
            $mime = finfo_file($finfo, $filename);
            if ($ext=='xlsx') $mime = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
            $titles = [];
            foreach ($types as $type) {
                $titles[]=$mimes[$type]['title'];
                if ($mimes[$type]['items'][$mime]) {
                    $arr['ext']=$mimes[$type]['items'][$mime];
                    $arr['type'] = $type;

                }
            }
            if (!$arr['ext']) {
                $arr = [
                    'status' => 'error',
                    'message' => 'Тип файла не поддержвивается. Выберите '.implode(', ', $titles) .' '.$mime
                ];
            }
        } else {
            $arr['status']='error';
            $arr['message']='Файл не найден';
        }
        return $arr;
    }

    public static function GetFilename($dir, $filename) {
        while (file_exists($dir.$filename))
        {
            $filename_array = explode('.',$filename);
            $filename = $filename_array[0].'_'.mt_rand(0,1000).'.'.$filename_array[count($filename_array)-1];
        }
        return $filename;
    }
    public static function SaveImage($source, $filename, $image_config, $rotate=0) {
        foreach ($image_config as $index1=>$value1)
        {
            $image_class = new \s\SimpleImage();
            $image_class->load($source);

            $image_class->rotate($rotate * (-1));

            if ($value1['crop']) {
                if ($value1['h'] > $value1['w']) {
                    $image_class->resizeToHeight($value1['h']);
                } else {
                    $image_class->resizeToWidth($value1['w']);
                }

                $image_class->crop(
                    round($image_class->getWidth() / 2 - $value1['w'] / 2),
                    round($image_class->getHeight() / 2 - $value1['h'] / 2),
                    $value1['w'],
                    $value1['h']
                );
            } else {
                $image_class->resizeToWidth($value1['w']);
                $image_class->resizeToHeight($value1['h']);
            }
            $image_class->save($value1['dir'].$filename);
        }
    }

    public static function LoadFileToTmp($file, $types=['zip', 'pdf', 'img', 'doc']) {
        $arr = Array('status' => 'ok');
        if ((isset($file['tmp_name']))&&($file['tmp_name'] != '')) {

            $filename_array = explode('.',$file['name']);
            $ext = $filename_array[count($filename_array)-1];

            $arr = self::CheckFile($file['tmp_name'], $types, $ext);
            if ($arr['status'] == 'ok') {
                $tmp_dir = $_SERVER['DOCUMENT_ROOT'] . '/tmp/';
                $filename = Forms::GetFilename($tmp_dir, time() . '.' . $arr['ext']);
                if (move_uploaded_file($file['tmp_name'], $tmp_dir . $filename)) {
                    $arr['title'] = $file['name'];
                    $arr['url'] = '/tmp/' . $filename;
                    $arr['filename'] = $filename;
                } else {
                    $arr['status'] = 'error';
                    $arr['message'] = 'Файл не загружен';
                }
            }
        } else {
            $arr['status'] = 'error';
            $arr['message'] = 'Файл не загружен. Возможно его размер больше ' . ini_get('upload_max_filesize');
        }
        return $arr;
    }
    public static function LoadFileFromTmp($filename, $dir, $types=['zip', 'pdf', 'img', 'doc'], $old='')
    {
        $name = preg_replace("/\(^[0-9a-zA-Z_]+\)/", "", $filename);
        $tmp_dir = $_SERVER['DOCUMENT_ROOT'] . '/tmp/';
        $src_filename = $tmp_dir . $name;

        $filename_array = explode('.', $filename);
        $ext = $filename_array[count($filename_array)-1];

        $arr = self::CheckFile($src_filename, $types, $ext);
        if ($arr['status']=='ok') {
            $dst_filename = Forms::GetFilename($dir, time().'.'.$arr['ext']);
            $arr['filename'] = $dst_filename;
            if (copy($src_filename, $dir . $dst_filename)) {
                $arr['url'] = $dir . $dst_filename;
                $arr['filename'] = $dst_filename;
            } else {
                $arr['status'] = 'error';
                $arr['message'] = 'Файл не загружен';
            }
            if ($old) {
                if (file_exists($dir . $old))
                    unlink($dir . $old);
            }
            unlink($tmp_dir . $name);
        }
        return $arr;
    }
}