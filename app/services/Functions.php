<?php
namespace s;

class Functions {
    public static function FixPhone($phoneNumber) {
        $phoneNumber = preg_replace('/[^0-9]/', '', $phoneNumber); // удалим пробелы, и прочие не нужные знаки
        if(is_numeric($phoneNumber))
        {
            if(strlen($phoneNumber) < 4) // если длина номера слишком короткая, вернем false
            {
                return FALSE;
            }
            else
            {
                if ((mb_substr($phoneNumber,0,2)=='89')&&(strlen($phoneNumber) == 11))
                {
                    $phoneNumber='79'.mb_substr($phoneNumber,2);
                }
                return floatval($phoneNumber);
            }
        }
        else
        {
            return FALSE;
        }
    }
    public static function FixEmail($email) {
        return mb_strtolower($email);
    }
    public static function load_file_url($url, $file)
    {
        $ch2 = curl_init($url);
        $fp = fopen($file, 'wb');
        curl_setopt($ch2, CURLOPT_HTTPHEADER, array('Accept-Language: ru,en-us'));
        curl_setopt($ch2, CURLOPT_FILE, $fp);
        curl_setopt($ch2, CURLOPT_HEADER, 0);
        curl_setopt($ch2, CURLOPT_CONNECTTIMEOUT, 20);
        curl_setopt($ch2, CURLOPT_TIMEOUT, 20);
        curl_exec($ch2);
        curl_close($ch2);
        fclose($fp);
    }
    public static function GetFileSource($url) {
        return $url.'?'.filemtime($_SERVER['DOCUMENT_ROOT'] . $url);
    }
    public static function getWord($number, $suffix) {
        $keys = array(2, 0, 1, 1, 1, 2);
        $mod = $number % 100;
        $suffix_key = ($mod > 7 && $mod < 20) ? 2: $keys[min($mod % 10, 5)];
        return $suffix[$suffix_key];
    }
    public static function getSrcScripts($index)
    {
        $s = file_get_contents(AppDir . '/../gulp_config.json');
        $a = json_decode($s);
        foreach ($a->js[$index]->src as $item) {
            $item = str_replace('**/', '', $item);
            $patch = glob(AppDir . '/../' . $item);
            foreach ($patch as $src) {
                $src = str_replace(AppDir . '/../public', '', $src);
                ?>
                <script type="text/javascript"
                        src="<?= self::GetFileSource($src) ?>"></script>
                <?php
            }
        }
    }
}