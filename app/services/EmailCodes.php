<?php
namespace s;

class EmailCodes extends \m\EmailCodes {
    public static function GetCode($hash, $email) {
        $email_codes = new EmailCodes();
        $email_codes->GetOne([
            'hash'=>$hash,
            'email'=>$email
        ]);
        if ($email_codes->id) {
            $arr=$email_codes->data['code'];
        } else {
            $arr['code'] = rand(1000, 9999);
            $arr['hash'] = self::GetHash($arr['code'].$email);
            $email_codes->Insert(['code'=>$arr['code'], 'hash'=>$arr['hash'], 'email'=>cstr($email)]);
        }
        return $arr;
    }
    public static function GetHash($str) {
        return md5($str.'retg53w');
    }
}