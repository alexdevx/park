<?php

namespace m;


class Cars extends \m\Models
{
    public $table_name='car_brand';
    public $id_name='car_brand_id';
    public $options = [
        'order'=>'`car_brand`.`car_brand` ASC'
    ];
}