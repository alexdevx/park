<?php
namespace m;

class EmailCodes extends Models {
    public $table_name='email_codes';
    public $id_name='email_code_id';
    public $options = [
        'order'=>'`email_codes`.`email_code_id` DESC'
    ];
}