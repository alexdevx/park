<?php
namespace m;
/**
 * Работа с посетителями
 * Class Visitors
 */
class Visitors extends \m\Models {
    public $table_name='visitors';
    public $id_name='visitor_id';
    public $options = [
        'order'=>'`visitors`.`date` DESC'
    ];
}
