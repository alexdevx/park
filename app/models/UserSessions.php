<?php
namespace m;
class UserSessions extends Models {
    public $table_name='user_sessions';
    public $id_name='user_session_id';
    public $options = [
        'order'=>'`user_sessions`.`user_session_id` DESC'
    ];
}