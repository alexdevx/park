<?php
$view['status'] = 'ok';
if ($_GET['id']) {
    $view['visitor'] = new \s\Visitors();
    if (is_numeric($_GET['id'])) {
        $view['visitor']->Get($_GET['id']);
        if (!$view['visitor']->id) {
            $view['status'] = 'error';
            $view['message'] = 'Посетитель не найден';
        } else {
            $view['car_brand'] = new \s\Cars($view['visitor']->data['car_brand_id']);
            $view['title'] = $view['visitor']->data['name'];
            $view['company'] = new \s\Users($view['visitor']->data['company_user_id']);
        }
        $view['btn_title'] = 'Сохранить';
    } else {
        $view['title'] = 'Заказать пропуск';
        $view['visitor']->data['date'] = date('Y-m-d', time()+3600*24);
        if ($user->data['type']==1) {
            $view['company'] = $user;
        } else {
            $view['company'] = new \s\Users($user->companies_ids[0]);
        }
        $view['btn_title'] = 'Заказать';
    }
}