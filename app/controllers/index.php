<?php
$view['menu_pages'] = new \s\Pages();
$params = ['del'=>0, 'menu'=>1, 'type'.$user->type=>1];
if (!$user->auth) {
    $params['user_auth'] = 0;
}
$view['menu_pages']->GetItems($params);

$view['footer_pages'] = new \s\Pages();
$view['footer_pages']->GetItems(['del'=>0, 'footer'=>1]);