<?php

header('Content-Type: application/vnd.ms-excel; format=attachment;');
header('Content-Disposition: attachment; filename=downloaded_' . date('Y-m-d') . '.xls');
header('Expires: Mon, 18 Jul 1998 01:00:00 GMT');
header('Cache-Control: no-store, no-cache, must-revalidate');
header('Cache-Control: post-check=0, pre-check=0', FALSE);
header('Pragma: no-cache');


ob_start();
$view['visitors'] = new \s\Visitors();
$countItems = 1000;
$params = [];
$options = [
    'field_sql' => "`visitors`.*, `users`.`name` as `company_name`, `car_brand`.*",
    'table_sql' => "LEFT JOIN `users` ON `users`.`user_id`=`visitors`.`company_user_id` LEFT JOIN `car_brand` ON `visitors`.`car_brand_id`=`car_brand`.`car_brand_id`",
    'limit_count' => $countItems
];
if ($_GET['find_text']) {
    $str = cstr($_GET['find_text']);
    $w = '';
    $a = explode(" ", $str);

    if (count($a) == 2) {
        $w .= " OR (`visitors`.`name` LIKE '" . $a[0] . "%' AND `visitors`.`fename` LIKE '" . $a[1] . "%') 
            OR (`visitors`.`name` LIKE '" . $a[1] . "%' AND `visitors`.`fename` LIKE '" . $a[0] . "%')";
    } else {
        foreach ($a as $str) {
            $int = intval($str);
            $w .= ($int ? " OR `visitors`.`visitor_id`=$int" : "") . " OR `visitors`.`name` LIKE '$str%' OR `visitors`.`fename` LIKE '$str%'";
        }
    }
    $options['where_sql'] .= " AND (0 $w)";
}
if ($_GET['find_car_brand'])
{
    $params['car_brand_id'] = intval($_GET['find_car_brand']);
}
if ($_GET['find_car_number'])
{
    $str = cstr($_GET['find_car_number']);
    $w = '';
    $a = explode(" ", $str);
    $w .= " OR (`visitors`.`car_number` LIKE '" . $str . "%') ";
    $options['where_sql'] .= " AND (0 $w)";
}
if ($_GET['find_weight'])
{
    $params['weight'] = intval($_GET['find_weight']);
}
if ($user->type == 1) {
    $options['where_sql'] .= " AND `visitors`.`company_user_id`=" . $user->id;
} elseif ($user->type == 0) {
    $options['where_sql'] .= " AND `visitors`.`company_user_id` IN (" . implode(',', $user->companies_ids) . ")";
}
if ($_GET['find_company_user_id']) {
    $params['company_user_id'] = intval($_GET['find_company_user_id']);
}
if ($_GET['find_status']) {
    if ($_GET['find_status']=='none') {
        $options['where_sql'] .= " AND `visitors`.`date_in` is NULL";
    } elseif ($_GET['find_status']=='in') {
        $options['where_sql'] .= " AND `visitors`.`date_in` is NOT NULL AND `visitors`.`date_out` is NULL";
    } elseif ($_GET['find_status']=='out') {
        $options['where_sql'] .= " AND `visitors`.`date_in` is NOT NULL AND `visitors`.`date_out` is NOT NULL";
    }
}
if ($_GET['find_date_from']) {
    $options['where_sql'] .= " AND `visitors`.`date` >= '".date('Y-m-d', strtotime($_GET['find_date_from']))."'";
}
if ($_GET['find_date_to']) {
    $options['where_sql'] .= " AND `visitors`.`date` <= '".date('Y-m-d', strtotime($_GET['find_date_to']))."'";
}
$rez = $view['visitors']->GetItems($params, $options);
$arr['i'] = $_GET['i'] + $countItems;
$arr['count'] = $view['visitors']->count;
foreach ($view['visitors']->items as $item) {
    ?>
    <tr class="ripple" data-id="<?= $item['visitor_id'] ?>" data-del="<?=$item['del']?>">
        <td><?= date('d.m.Y', strtotime($item['date'])) ?></td>
        <td><?= $item['company_name'] ?></td>
        <td><?= $item['name'] ?> <?= $item['fename'] ?></td>
        <td><?= $item['car_brand'] ?></td>
        <td><?= $item['car_number'] ?></td>
        <td><?= $item['weight'] == 0 ? 'До 3.5т' : 'Больше 3.5т' ?></td>
        <td><?= $item['passengers'] ?></td>
        <td><?= date('d.m.Y', strtotime($item['date_add'])) ?></td>
        <td><?= ($item['date_in'] ? date('d.m.Y', strtotime($item['date_in'])) : '') ?></td>
        <td><?= ($item['date_out'] ? date('d.m.Y', strtotime($item['date_out'])) : '') ?></td>
    </tr>
    <?php
}
$arr['html'] = ob_get_contents();
ob_clean();
?>

<meta http-equiv="content-type" content="text/html; charset=utf-8">

<table class="table">
    <thead>
    <tr>
        <th>Назначено</th>
        <th>Компания</th>
        <th>Посетитель</th>
        <th>Марка авто</th>
        <th>Номер авто</th>
        <th>Вес авто</th>
        <th>Пассажиры</th>
        <th>Добавлен</th>
        <th>Вошел</th>
        <th>Вышел</th>
    </tr>
    </thead>
    <tbody>
    <?php echo $arr['html'];?>
    </tbody>
</table>