<?php
$arr = ['status'=>'ok'];
$file = AppDir.'/controllers/ajax/'.$this->urls[1].'/'.$this->urls[2].'.php';
if (file_exists($file)) {
    include $file;
} else {
    $arr['status'] = 'error';
    $arr['message'] = 'Not Found';
}
echo json_encode_x($arr);