<?php

header('Content-Type: application/vnd.ms-excel; format=attachment;');
header('Content-Disposition: attachment; filename=downloaded_' . date('Y-m-d') . '.xls');
header('Expires: Mon, 18 Jul 1998 01:00:00 GMT');
header('Cache-Control: no-store, no-cache, must-revalidate');
header('Cache-Control: post-check=0, pre-check=0', FALSE);
header('Pragma: no-cache');

    ob_start();
    $view['visitors'] = new \s\Visitors();
    
    $params = [];
    $options = [
        'field_sql' => "`visitors`.*, `users`.`name` as `company_name`",
        'table_sql' => "LEFT JOIN `users` ON `users`.`user_id`=`visitors`.`company_user_id`",
        
    ];
    if ($_GET['find_text']!='') {
        $str = cstr($_GET['find_text']);
        $w = '';
        $a = explode(" ", $str);

        if (count($a) == 2) {
            $w .= " OR (`visitors`.`name` LIKE '" . $a[0] . "%' AND `visitors`.`fename` LIKE '" . $a[1] . "%') 
                OR (`visitors`.`name` LIKE '" . $a[1] . "%' AND `visitors`.`fename` LIKE '" . $a[0] . "%')";
        } else {
            foreach ($a as $str) {
                $int = intval($str);
                $w .= ($int ? " OR `visitors`.`visitor_id`=$int" : "") . " OR `visitors`.`name` LIKE '$str%' OR `visitors`.`fename` LIKE '$str%'";
            }
        }
        $options['where_sql'] .= " AND (0 $w)";
    }
    if ($_GET['admin_name']!='') {
        $str = cstr($_GET['admin_name']);
        $w = '';
        $a = explode(" ", $str);
        $w .= " OR (`visitors`.`admin_name` LIKE '" . $str . "%') ";
        $options['where_sql'] .= " AND (0 $w)";
    }
    if ($user->type == 1) {
        $options['where_sql'] .= " AND `visitors`.`company_user_id`=" . $user->id;
    } elseif ($user->type == 0) {
        $options['where_sql'] .= " AND `visitors`.`company_user_id` IN (" . implode(',', $user->companies_ids) . ")";
    }
    if ($_GET['find_company_user_id']!=NULL && $_GET['find_company_user_id']!='') {
        $params['company_user_id'] = intval($_GET['find_company_user_id']);
    }
    if ($_GET['find_status']!='') {
        if ($_GET['find_status']=='none') {
            $options['where_sql'] .= " AND `visitors`.`date_in` is NULL";
        } elseif ($_GET['find_status']=='in') {
            $options['where_sql'] .= " AND `visitors`.`date_in` is NOT NULL AND `visitors`.`date_out` is NULL";
        } elseif ($_GET['find_status']=='out') {
            $options['where_sql'] .= " AND `visitors`.`date_in` is NOT NULL AND `visitors`.`date_out` is NOT NULL";
        }
    }
    if ($_GET['find_date_from']!='') {
        $options['where_sql'] .= " AND `visitors`.`date` >= '".date('Y-m-d', strtotime($_GET['find_date_from']))."'";
    }
    if ($_GET['find_date_to']!='') {
        $options['where_sql'] .= " AND `visitors`.`date` <= '".date('Y-m-d', strtotime($_GET['find_date_to']))."'";
    }
    $rez = $view['visitors']->GetItems($params, $options);
    $arr['i'] = $countItems;
    $arr['count'] = $view['visitors']->count;
    foreach ($view['visitors']->items as $item) {
        ?>
        <tr class="ripple" data-id="<?= $item['visitor_id'] ?>" data-del="<?=$item['del']?>">
            <td><?= date('d.m.Y', strtotime($item['date'])) ?></td>
            <td><?= $item['company_name'] ?></td>
            <td><?= $item['name'] ?> <?= $item['fename'] ?></td>
            <td><?= ($item['date_in'] ? date('d.m.Y H:i:s', strtotime($item['date_in'])) : '') ?></td>
            <td><?= ($item['date_out'] ? date('d.m.Y H:i:s', strtotime($item['date_out'])) : '') ?></td>
        </tr>
        <?php
    }
    $arr['html'] = ob_get_contents();
    ob_clean();
    ?>
<style>
    table {
        width: 100%; /* Ширина таблицы */
        border: 3px solid black; /* Рамка вокруг таблицы */
        border-collapse: collapse; /* Отображать только одинарные линии */
    }
    th {
        padding: 5px; /* Поля вокруг содержимого ячеек */
        border: 2px solid black; /* Граница вокруг ячеек */
    }
    td {
        padding: 5px; /* Поля вокруг содержимого ячеек */
        border: 1px solid black; /* Граница вокруг ячеек */
    }
</style>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">

    <table class="table">
        <thead>
        <tr>
            <th>Назначено</th>
            <th>Компания</th>
            <th>Посетитель</th>
            <th>Вошел</th>
            <th>Вышел</th>
        </tr>
        </thead>
        <tbody>
        <?php echo $arr['html'];?>
        </tbody>
    </table>
