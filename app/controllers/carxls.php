<?php

header('Content-Type: application/vnd.ms-excel; format=attachment;');
header('Content-Disposition: attachment; filename=downloaded_' . date('Y-m-d') . '.xls');
header('Expires: Mon, 18 Jul 1998 01:00:00 GMT');
header('Cache-Control: no-store, no-cache, must-revalidate');
header('Cache-Control: post-check=0, pre-check=0', FALSE);
header('Pragma: no-cache');


ob_start();
$view['visitors'] = new \s\Visitors();
$countItems = 1000;
$params = [];
$weight = array(0=>'До 3.5т', 1=>'От 3.5т до 10т', 2=>'Больше 10т');
$options = [
    'field_sql' => "`visitors`.*, `users`.`name` as `company_name`, `car_brand`.*",
    'table_sql' => "LEFT JOIN `users` ON `users`.`user_id`=`visitors`.`company_user_id` LEFT JOIN `car_brand` ON `visitors`.`car_brand_id`=`car_brand`.`car_brand_id`",
    'limit_count' => $countItems
];
if ($_GET['find_text']) {
    $str = cstr($_GET['find_text']);
    $w = '';
    $a = explode(" ", $str);

    if (count($a) == 2) {
        $w .= " OR (`visitors`.`name` LIKE '" . $a[0] . "%' AND `visitors`.`fename` LIKE '" . $a[1] . "%') 
            OR (`visitors`.`name` LIKE '" . $a[1] . "%' AND `visitors`.`fename` LIKE '" . $a[0] . "%')";
    } else {
        foreach ($a as $str) {
            $int = intval($str);
            $w .= ($int ? " OR `visitors`.`visitor_id`=$int" : "") . " OR `visitors`.`name` LIKE '$str%' OR `visitors`.`fename` LIKE '$str%'";
        }
    }
    $options['where_sql'] .= " AND (0 $w)";
}
if ($_GET['admin_name']!='') {
    $str = cstr($_GET['admin_name']);
    $w = '';
    $a = explode(" ", $str);
    $w .= " OR (`visitors`.`admin_name` LIKE '" . $str . "%') ";
    $options['where_sql'] .= " AND (0 $w)";
}
if ($_GET['find_car_brand'])
{
    $params['car_brand_id'] = intval($_GET['find_car_brand']);
}
if ($_GET['find_car_number'])
{
    $str = cstr($_GET['find_car_number']);
    $w = '';
    $a = explode(" ", $str);
    $w .= " OR (`visitors`.`car_number` LIKE '" . $str . "%') ";
    $options['where_sql'] .= " AND (0 $w)";
}
if ($_GET['weight']!='none')
{
    $options['where_sql'] .= " AND `visitors`.`weight`=" . intval($_GET['weight']);
}
if ($_GET['time_out']==1)
{
    $options['where_sql'] .= " AND HOUR(`date_out`)>0 AND HOUR(`date_out`)<6";
}
$options['where_sql'] .= " AND `visitors`.`car_number`!=''";
if ($user->type == 1) {
    $options['where_sql'] .= " AND `visitors`.`company_user_id`=" . $user->id;
} elseif ($user->type == 0) {
    $options['where_sql'] .= " AND `visitors`.`company_user_id` IN (" . implode(',', $user->companies_ids) . ")";
}
if (isset($_GET['find_company_user_id']) && $_GET['find_company_user_id'] != null) {
    $params['company_user_id'] = intval($_GET['find_company_user_id']);
}
if ($_GET['find_status']) {
    if ($_GET['find_status']=='none') {
        $options['where_sql'] .= " AND `visitors`.`date_in` is NULL";
    } elseif ($_GET['find_status']=='inner') {
        $options['where_sql'] .= " AND `visitors`.`date_in` is NOT NULL";
    } elseif ($_GET['find_status']=='in') {
        $options['where_sql'] .= " AND `visitors`.`date_in` is NOT NULL AND `visitors`.`date_out` is NULL";
    } elseif ($_GET['find_status']=='out') {
        $options['where_sql'] .= " AND `visitors`.`date_in` is NOT NULL AND `visitors`.`date_out` is NOT NULL";
    }
}
if ($_GET['find_date_from']) {
    $options['where_sql'] .= " AND `visitors`.`date` >= '".date('Y-m-d', strtotime($_GET['find_date_from']))."'";
}
if ($_GET['find_date_to']) {
    $options['where_sql'] .= " AND `visitors`.`date` <= '".date('Y-m-d', strtotime($_GET['find_date_to']))."'";
}
$rez = $view['visitors']->GetItems($params, $options);
$arr['i'] = $_GET['i'] + $countItems;
$arr['count'] = $view['visitors']->count;
foreach ($view['visitors']->items as $item) {
    $date_out = strtotime(date('Y-m-d', strtotime($item['date_out'])));
    $date_in = strtotime(date('Y-m-d', strtotime($item['date_in'])));
    if ($item['date_out']) {
		$days_on_parking = ($date_out - $date_in) / (24 * 3600);
    } else {
		$days_on_parking = 0;
    }
    ?>
    <style>
        table {
            width: 100%; /* Ширина таблицы */
            border: 3px solid black; /* Рамка вокруг таблицы */
            border-collapse: collapse; /* Отображать только одинарные линии */
        }
        th {
            padding: 5px; /* Поля вокруг содержимого ячеек */
            border: 2px solid black; /* Граница вокруг ячеек */
        }
        td {
            padding: 5px; /* Поля вокруг содержимого ячеек */
            border: 1px solid black; /* Граница вокруг ячеек */
        }
    </style>
    <tr class="ripple" data-id="<?= $item['visitor_id'] ?>" data-del="<?=$item['del']?>">
        <td><?= $item['name'] ?> <?= $item['fename'] ?></td>
        <td><?= ($item['date_in'] ? date('M', strtotime($item['date_in'])) : '') ?></td>
        <td><?= ($item['date_in'] ? date('d.m.Y', strtotime($item['date_in'])) : '') ?></td>
        <td><?= ($item['date_in'] ? date('H:i:s', strtotime($item['date_in'])) : '') ?></td>
        <td><?= ($item['date_out'] ? date('d.m.Y', strtotime($item['date_out'])) : '') ?></td>
        <td><?= ($item['date_out'] ? date('H:i:s', strtotime($item['date_out'])) : '') ?></td>
        <td><?= $item['company_name'] ?></td>
        <td><?= $item['admin_name'] ?></td>
        <td><?= $item['car_brand'] ?> <?= $item['car_number'] ?></td>
        <td><?= $item['admin_name'] ?>, <?= date('d.m.Y', strtotime($item['date_add'])) ?></td>
        <td><?= $weight[$item['weight']] ?></td>
        <td><?= $item['passengers'] ?></td>
        <td><?= $days_on_parking ?></td>
    </tr>
    <?php
}
$arr['html'] = ob_get_contents();
ob_clean();
?>

<meta http-equiv="content-type" content="text/html; charset=utf-8">

<table class="table">
    <thead>
    <tr>
        <th>Водитель</th>
        <th>Месяц</th>
        <th>Дата въезда</th>
        <th>Время въезда</th>
        <th>Дата выезда</th>
        <th>Время выезда</th>
        <th>ФИРМА</th>
        <th>представит. Фирмы</th>
        <th>Авто</th>
        <th>Добавил</th>
        <th>Вес авто</th>
        <th>Пассажиры</th>
        <th>Дни</th>
    </tr>
    </thead>
    <tbody>
    <?php echo $arr['html'];?>
    </tbody>
</table>