<?php
$view['status'] = 'ok';
if ($_GET['id']) {
    $view['user'] = new \s\Users();
    if (is_numeric($_GET['id'])) {
        $view['user']->Get($_GET['id']);
        if (!$view['user']->id) {
            $view['status'] = 'error';
            $view['message'] = 'Пользователь не найден';
        } else {
            $view['title'] = $view['user']->data['name'];
        }
    } else {
        $view['title'] = 'Добавить сотрудника';
        $view['user']->data['active']=1;
        $view['user']->data['type']=1;
    }
}