<?php
if (!$user->auth) {
    $arr['status'] = 'error';
    $arr['message'] = 'Вы не авторизованы';
} elseif ($user->data['active']==0) {
    $arr['status'] = 'error';
    $arr['message'] = 'Ваш аккаунт заморожен';
} elseif ($user->type!=1) {
    $arr['status'] = 'error';
    $arr['message'] = 'Нет доступа';
}
if ($arr['status'] == 'ok') {
    $u = new \s\Users($_POST['user_id']);
    if (!$u->id) {
        $arr['status'] = 'error';
        $arr['message'] = 'Сотрудник не найден';
    }
}
if ($arr['status'] == 'ok') {
    $user2user = new \s\User2User();
    $user2user->GetItems([
        'to_user_id'=>$u->id,
        'user_id'=>$user->id
    ], [
        'table_sql'=>", `users`",
        'where_sql'=>"AND `user2user`.`user_id`=`users`.`user_id`"
    ]);
    if ($user2user->count==0) {
        $arr['status'] = 'error';
        $arr['message'] = 'Нет доступа к данному сотруднику';
    }
}
if ($arr['status'] == 'ok') {
    $u->Disable();
}