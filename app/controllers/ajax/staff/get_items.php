<?php
if (!$user->auth) {
    $arr['status'] = 'error';
    $arr['message'] = 'Вы не авторизованы';
} elseif ($user->data['active']==0) {
    $arr['status'] = 'error';
    $arr['message'] = 'Ваш аккаунт заморожен';
}
if ($arr['status'] == 'ok') {
    $countItems = 30;
    ob_start();
    $view['users'] = new \s\Users();
    $params = ['del' => 0];
    $options = ['limit_count' => $countItems, 'limit_from' => $_POST['i']];
    if ($_POST['find']['text']) {
        $str = cstr($_POST['find']['text']);
        $w = '';
        $a = explode(" ", $str);

        if (count($a) == 2) {
            $w .= " OR (`name` LIKE '" . $a[0] . "%' AND `fename` LIKE '" . $a[1] . "%') OR (`name` LIKE '" . $a[1] . "%' AND `fename` LIKE '" . $a[0] . "%')";
        } else {
            foreach ($a as $str) {
                $int = intval($str);
                $w .= ($int ? " OR `user_id`=$int" : "") . " OR `phone` LIKE '%$str%' OR `email`='$str%' OR `name` LIKE '$str%' OR `fename` LIKE '$str%'";
            }
        }
        $options['where_sql'] .= " AND (0 $w)";
    }
    $options['field_sql'] = '`users`.*';
    $options['table_sql'] .= ", `user2user`";
    $options['where_sql'] .= " AND `user2user`.`to_user_id`=`users`.`user_id` AND `user2user`.`user_id`=" . $user->id;
    $rez = $view['users']->GetItems($params, $options);
    $arr['i'] = $_POST['i'] + $countItems;
    $arr['count'] = $view['users']->count;
    foreach ($view['users']->items as $item) {
        ?>
        <a href="?id=<?= $item['user_id'] ?>" class="item ripple" rel="0" data-id="<?= $item['user_id'] ?>">
            <div class="image"
                 style="<?= ($item['image0'] ? "background-image: url('" . $view['users']->image_config[2]['url'] . $item['image0'] . "');" : '') ?>"></div>
            <div class="item_inner">
                <div class="title"><?= $item['name'] ?></div>
                <div class="right"><?= $item['email'] ?></div>
            </div>
        </a>
        <?php
    }
    $arr['html'] = ob_get_contents();
    ob_clean();
}