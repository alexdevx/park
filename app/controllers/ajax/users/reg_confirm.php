<?php
$arr = s\Users::Check_Reg($_POST);
if ($arr['status']=='ok') {
    $email = s\Functions::FixEmail($_POST['email']);
    if (s\EmailCodes::GetHash($_POST['code'].$email)==$_POST['hash']) {
        $params = [
            'name'=>$_POST['name'],
            'email'=>$email,
            'pass'=>\s\Users::GetPass($_POST['pass'])
        ];
        $users = new \s\Users();
        $users->GetOne([
            'email'=>$email,
            'del'=>0
        ]);
        if ($users->id) {
            $arr['status']='error';
            $arr['message']='Пользователь с таким E-mail уже существует';
        } else {
            $users->Insert($params, true);
            $users->AddAuth();
            $arr['message'] = 'Вы успешно зарегистрировались';
        }
    } else {
        $arr['status']='error';
        $arr['message'] = 'Неправильный код из смс';
    }
}