<?php
$users = new \s\Users();
$users->GetOne([
    'email'=>\s\Functions::FixEmail($_POST['email']),
    'del'=>0
]);
if ($users->id) {
    $users->Send_email_forgot();
} else {
    $arr['status'] = 'error';
    $arr['message'] = 'Пользователя с таким E-mail не найдено';
}