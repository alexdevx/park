<?php
if (!$user->auth) {
    $arr['status'] = 'error';
    $arr['message'] = 'Вы не авторизованы';
} elseif ($user->type!=3) {
    $arr['status'] = 'error';
    $arr['message'] = 'Нет доступа';
} else {
    if (!$_POST['name']) {
        $arr['status'] = 'error';
        $arr['message'] = 'Введите имя';
    } elseif (!$_POST['email']) {
        $arr['status'] = 'error';
        $arr['message'] = 'Введите E-mail';
    }
}

if ($arr['status'] == 'ok') {
    $u = new \s\Users();
    $user2user = new \s\User2User();
    if ($_POST['user_id']) {
        $u->Get($_POST['user_id']);
        if (!$u->id) {
            $arr['status'] = 'error';
            $arr['message'] = 'Пользователь не найден';
        } else {
            $user2user->GetItems([
                'to_user_id'=>$u->id
            ], [
                'table_sql'=>", `users`",
                'where_sql'=>"AND `user2user`.`user_id`=`users`.`user_id`"
            ]);
            if ($u->data['email'] != $_POST['email']){
                $check_login = \s\Users::checkLoginName($_POST['email']);
                if ($check_login){
                    $arr['status'] = 'error';
                    $arr['message'] = 'Такой логин уже используется. Введите другой логин.';
                }
            }
        }
    } else {
        if (!$_POST['pass']) {
            $arr['status'] = 'error';
            $arr['message'] = 'Введите пароль';
        }
        $checkLogin = \s\Users::checkLoginName($_POST['email']);
        if ($checkLogin){
           
            $arr['status'] = 'error';
            $arr['message'] = 'Такой логин уже используется. Введите другой логин.';
        }
    }
}

if ($arr['status'] == 'ok') {
    $active = cstr($_POST['active']);
    $show_car = cstr($_POST['show_car']);
    if (!isset($_POST['active'])) {
        $active = 0;
    }
    if (!isset($_POST['show_car'])) {
        $show_car = 0;
    }
    $params = [
        'name'=>cstr($_POST['name']),
        'phone'=>cstr($_POST['phone']),
        'email'=>cstr($_POST['email']),
        'type'=>intval($_POST['type']),
        'active'=>$active,
        'show_car'=>$show_car
    ];

    if ($_POST['pass']) {
        $params['pass'] = \s\Users::GetPass($_POST['pass']);
    }
    if ($_POST['image']['crop']) {
        $image = \s\Forms::LoadFromTmp($_POST['image']['crop'], $u->ImageCfg(), $u->data['image0']);
        $params['image0'] = $image['filename'];
    }
    if ($u->id) {
        $u->Update($params);
    } else {
        $u->Insert($params, true);
    }
    if ($_POST['user2user']) {
        foreach ($_POST['user2user'] as $v) {
            if (!$user2user->items2id[$v]) {
                $user2user->Insert([
                    'user_id'=>$v,
                    'to_user_id'=>$u->id
                ]);
            }
        }
    }

    foreach ($user2user->items as $v) {
        if ((!$_POST['user2user'])||(!in_array($v['user_id'], $_POST['user2user']))) {
            $user2user->Delete($v['user_id'], $u->id);
        }
    }
}