<?php
if (!$user->auth) {
    $arr['status'] = 'error';
    $arr['message'] = 'Вы не авторизованы';
}
if ($arr['status'] == 'ok') {
    if ($user->type == 3){
        $user->Update([
            'name'=>cstr($_POST['name']),
            'fename'=>cstr($_POST['fename'])
        ]);
    }else{
        $arr['status'] = 'error';
        $arr['message'] = 'Менять имя может только администратор';
    }
    
}