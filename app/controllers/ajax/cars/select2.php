<?php
$per_page = 20;
$arr = ['status'=>'ok', 'reason'=>'', 'incomplete_results' => false, 'items' => array(), 'total_count' => 0];
if (!$user->auth) {
    $arr['status'] = 'error';
    $arr['message'] = 'Вы не авторизованы';
} elseif ($user->data['active']==0) {
    $arr['status'] = 'error';
    $arr['message'] = 'Ваш аккаунт заморожен';
}
if ($arr['status'] == 'ok') {
    $str = cstr($_GET['q']);
    $options = [
        'limit_from' => $per_page * intval($_GET['page']),
        'limit_count' => 20
    ];
    if (($str == '') && ($_GET['preload'])) {
        /*switch ($_GET['preload']) {
            case 'showman':
                $users = new \s\Users();
                $users->GetShowmanMk($domain->group->id, $options);
                $arr['total_count'] = $users->count;
                foreach ($users->items as $item) {
                    $arr['items'][] = [
                        'id' => $item['user_id'],
                        'text' => $item['name'] . ' ' . $item['fename'],
                        'description' => 'https://vk.com/id' . $item['vk_user_id'],
                        'image' => $item['photo_50']
                    ];
                }
                break;
        }*/
    } else {

        if ($str != '') {
            $w = '';
            $a = explode(" ", $str);

            $w .= " OR (`car_brand` LIKE '" . $a[0] . "%')";
            $options['where_sql'] .= " AND (0 $w)";
        }

        $cars = new \s\Cars();
        $params = [];
        $r = $cars->GetItems($params, $options);

        $arr['total_count'] = $cars->count;
        foreach ($cars->items as $item) {
            $arr['items'][] = [
                'id' => $item['car_brand_id'],
                'text' => $item['car_brand']
            ];
        }
    }
}