<?php
if (!$user->auth) {
    $arr['status'] = 'error';
    $arr['message'] = 'Вы не авторизованы';
} elseif ($user->data['active']==0) {
    $arr['status'] = 'error';
    $arr['message'] = 'Ваш аккаунт заморожен';
} elseif (!in_array($user->type, [2,3])) {
    $arr['status'] = 'error';
    $arr['message'] = 'Нет доступа';
}
if ($arr['status'] == 'ok') {
    $visitor = new \s\Visitors($_POST['visitor_id']);
    if (!$visitor->id) {
        $arr['status'] = 'error';
        $arr['message'] = 'Посетитель не найден';
    } elseif (!(($visitor->data['date']==date('Y-m-d'))||(($visitor->data['date_in'])&&(!$visitor->data['date_out'])))) {
        $arr['status'] = 'error';
        $arr['message'] = 'Нет доступа к посетителю';
    }
}
if ($arr['status'] == 'ok') {
    $params = [];
    $arr['status_text'] = '';
    $arr['status_name'] = '';
    $arr['btn_text'] = 'Впустить';
    if ($_POST['status']=='none') {
        if (!$visitor->data['date_in']) {
            $params['date_in'] = date('Y-m-d H:i:s');
            $visitor->data['date_in'] = $params['date_in'];
        }
        $arr['status_text'] = 'Вошел<br/> ' . \s\Visitors::FormatDate($visitor->data['date_in']);
        $arr['status_name'] = 'in';
        $params['in_user_id'] = $user->id;
        $arr['btn_text'] = 'Выпустить';
    } elseif (in_array($_POST['status'], ['in', 'out'])) {
        if (!$visitor->data['date_out']) {
            $params['date_out'] = date('Y-m-d H:i:s');
            $params['out_user_id'] = $user->id;
            $visitor->data['date_out'] = $params['date_out'];
        }
        $arr['status_text'] = 'Вышел<br/>' .\s\Visitors::FormatDate($visitor->data['date_out']);
        $arr['status_name'] = 'out';
        $arr['btn_text'] = '';
    }
    if ($_POST['car_number']) {
		$params['car_number'] = $_POST['car_number'];
	}
	if ($_POST['weight_id']) {
		$params['weight'] = $_POST['weight_id'];
	}
    if (count($params)) {
        $visitor->Update($params);
    }
}