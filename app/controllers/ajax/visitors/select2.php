<?php
$per_page = 20;
$arr = ['status'=>'ok', 'reason'=>'', 'incomplete_results' => false, 'items' => array(), 'total_count' => 0];
if (!$user->auth) {
    $arr['status'] = 'error';
    $arr['message'] = 'Вы не авторизованы';
} elseif ($user->data['active']==0) {
    $arr['status'] = 'error';
    $arr['message'] = 'Ваш аккаунт заморожен';
}
if ($arr['status'] == 'ok') {
    $str = cstr($_GET['q']);
    $options = [
        'limit_from' => $per_page * intval($_GET['page']),
        'limit_count' => 20
    ];
    if (($str == '') && ($_GET['preload'])) {
        /*switch ($_GET['preload']) {
            case 'showman':
                $users = new \s\Users();
                $users->GetShowmanMk($domain->group->id, $options);
                $arr['total_count'] = $users->count;
                foreach ($users->items as $item) {
                    $arr['items'][] = [
                        'id' => $item['user_id'],
                        'text' => $item['name'] . ' ' . $item['fename'],
                        'description' => 'https://vk.com/id' . $item['vk_user_id'],
                        'image' => $item['photo_50']
                    ];
                }
                break;
        }*/
    } else {

        if ($str != '') {
            $w = '';
            $a = explode(" ", $str);

            if (count($a) == 2) {
                $w .= " OR (`name` LIKE '" . $a[0] . "%' AND `fename` LIKE '" . $a[1] . "%') OR (`name` LIKE '" . $a[1] . "%' AND `fename` LIKE '" . $a[0] . "%')";
            } else {
                foreach ($a as $str) {
                    $int = intval($str);
                    $w .= ($int ? " OR `visitor_id`=$int" : "") . " OR `phone` LIKE '%$str%' OR `name` LIKE '$str%' OR `fename` LIKE '$str%'";
                }
            }
            $options['where_sql'] .= " AND (0 $w)";
        }

        $users = new \s\Visitors();
        $params = [];
        if ($_GET['params']['company_user_id']) {
            $params['company_user_id'] = intval($_GET['params']['company_user_id']);
        }
        if ($user->type == 1) {
            $options['where_sql'] .= " AND `company_user_id`=" . $user->id;
        } elseif ($user->type == 0) {
            $options['where_sql'] .= " AND `company_user_id` IN (" . implode(',', $user->companies_ids) . ")";
        }

        $r = $users->GetItems($params, $options);
        $arr['total_count'] = $users->count;
        foreach ($users->items as $item) {
            $arr['items'][] = [
                'id' => $item['user_id'],
                'text' => $item['name'] . ' ' . $item['fename'],
                'description' => date('d.m.Y', strtotime($item['date'])),
                'image' => ($item['image0'] ? $user->ImageCfg()[2]['url'] . $item['image0'] : '/images/user_noimg_50.png')
            ];
        }
    }
}