<?php
if (!$user->auth) {
    $arr['status'] = 'error';
    $arr['message'] = 'Вы не авторизованы';
}
if ($arr['status'] == 'ok') {
    $u = new \s\Visitors($_POST['visitor_id']);
    if ($u->id) {
        $r = $u->GetRight();
        if ($r['status']!='ok') {
            $arr['status'] = 'error';
            $arr['message'] = $r['message'];
        }
    } else {
        $arr['status'] = 'error';
        $arr['message'] = 'Пользователь не найден';
    }
}
if ($arr['status'] == 'ok') {
    $u->Update(['del'=>intval($_POST['del'])]);
}