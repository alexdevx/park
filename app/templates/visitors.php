<?php if ($view['visitor']) { ?>
<h1 class="page_title"><a href="?">Посетители</a> > <?=$view['title']?></h1>
<div class="VisitorsAdmin_Edit">
    <input type="hidden" name="visitor_id" value="<?=$view['visitor']->id?>"/>
    <div class="card_form">
        <div class="form_item">
            <label class="form_control">Имя</label>
            <input class="form_control" value="<?=$view['visitor']->data['name']?>" type="text" name="name"/>
        </div>
        <div class="form_item">
            <label class="form_control">Фамилия</label>
            <input class="form_control" value="<?=$view['visitor']->data['fename']?>" type="text" name="fename"/>
        </div>
        <div class="form_item">
            <label class="form_control">
                <?php if (!$view['visitor']->id) { ?>
                    <input type="radio" name="type_date" checked="checked" value="0"/>
                <?php } ?>
                Дата
            </label>
            <input class="form_control datepicker" value="<?=date('d.m.Y', strtotime($view['visitor']->data['date']))?>" style="width:120px;" type="text" name="date"/>
        </div>
        <?php if (!$view['visitor']->id) { ?>
            <div class="form_item">
                <label class="form_control">
                    <input type="radio" name="type_date" value="1"/>
                    Период
                </label>
                <input class="form_control datepicker"
                       value="<?= date('d.m.Y', strtotime($view['visitor']->data['date'])) ?>" style="width:120px;"
                       type="text" name="date_from"/> -
                <input class="form_control datepicker"
                       value="<?= date('d.m.Y', strtotime($view['visitor']->data['date'])+3600*24) ?>" style="width:120px;"
                       type="text" name="date_to"/>
            </div>
            <?php
        }
        if ($user->type==3) { ?>
            <div class="form_item">
                <label class="form_control">Компания</label>
                <select class="form_control" name="company_user_id" data-placeholder="Компания"
                        data-params='{"type":1}'>
                    <?php if ($view['company']->id) { ?>
                        <option selected="selected"
                                value="<?= $view['company']->id ?>"><?= $view['company']->data['name'] ?></option>
                    <?php } ?>
                </select>
            </div>
            <?php
        }
        if ($view['visitor']->data['add_user_id']) {
            $add_user = new \s\Users($view['visitor']->data['add_user_id']);
            ?>
            <div class="form_item">
                <label class="form_control">Добавил</label>
                <a <?=($user->type==3 ? 'href="/users?id='.$add_user->id.'"' : '')?> class="form_data _user">
                    <div class="img" style="<?=($add_user->data['image0'] ?  'background-image: url("'.$add_user->image_config[2]['url'].$add_user->data['image0'].'");' : '')?>"></div>
                    <div class="info">
                        <div class="name"><?=$add_user->data['name']?></div>
                        <div class="date"><?=date('d.m.Y', strtotime($view['visitor']->data['date_add']))?></div>
                    </div>
                </a>
            </div>
        <?php } ?>
        <?php if ($view['visitor']->data['date_in']) {
            ?>
            <div class="form_item">
                <label class="form_control">Впустил:</label>
                <div class="form_data">
                    <?php
                    if ($view['visitor']->data['in_user_id']) {
                        $in_user = new \s\Users($view['visitor']->data['in_user_id']);
                        ?>
                        <div>Вахтер: <?=$in_user->data['name']?></div>
                        <?php
                    }
                    ?>
                    <?=date('d.m.Y H:i', strtotime($view['visitor']->data['date_in']))?>
                </div>
            </div>
        <?php } ?>
        <?php if ($view['visitor']->data['date_out']) { ?>
            <div class="form_item">
                <label class="form_control">Выпустил:</label>
                <div class="form_data">
                    <?php
                    if ($view['visitor']->data['out_user_id']) {
                        $out_user = new \s\Users($view['visitor']->data['out_user_id']);
                        ?>
                        <div>Вахтер: <?=$out_user->data['name']?></div>
                        <?php
                    }
                    ?>
                    <?=date('d.m.Y H:i', strtotime($view['visitor']->data['date_out']))?>
                </div>
            </div>
        <?php } ?>
    </div>
    <div class="error"></div>
    <?php if ($view['visitor']->id) { ?>
        <div class="form_button _cancel_form red delete" data-del="<?=$view['visitor']->data['del']?>">
            <?=($view['visitor']->data['del']==1 ? 'Восстановить' : 'Удалить')?>
        </div>
    <?php } ?>
    <duv class="form_button _save_form save"><?=$view['btn_title']?></duv>
    <div style="clear:both;"></div>
</div>
<?php } else { ?>
    <h1 class="page_title">Посетители</h1>
    <div class="LineItems VisitorsAdmin">
        <div class="items_finder">
            <div class="item_finder text_search">
                <input value="" class="text" placeholder="Поиск администратора" name="admin_name" type="text"/>
                <div class="find_btn"><i class="fa fa-search" aria-hidden="true"></i></div>
            </div>
            <div class="item_finder text_search">
                <input value="<?=cstr($_GET['text'])?>" class="text" name="str_user" placeholder="Поиск посетителя" type="text"/>
                <div class="find_btn"><i class="fa fa-search" aria-hidden="true"></i></div>
            </div>
            <?php if ($user->type==3) { ?>
                <div class="item_finder not_grow">
                    <select style="min-width:250px;" name="company_user_id" data-placeholder="По всем арендаторам" data-params='{"type":1}'">

                    </select>
                </div>
            <?php } ?>
            <div class="item_finder not_grow">
                <input style="width:120px;" type="text" name="date_from" placeholder="Дата от" class="datepicker"/> -
                <input style="width:120px;" type="text" name="date_to" placeholder="Дата до" class="datepicker"/>
            </div>
            <div class="item_finder not_grow" style="flex-shrink: 1;">
                <select class="mini" name="status" style="width:100%;">
                    <option value="0">Статус</option>
                    <option value="none">Не вошедшие</option>
                    <option value="in">В здании</option>
                    <option value="out">Вышедшие</option>
                </select>
            </div>
            <a href="/xls" id="download_xls">Скачать xls</a>
        </div>
        <?php if ($user->type!=3) { ?>
            <a href="?id=add" class="form_button" style="margin-top: 20px;">Заказать пропуск</a>
            <div class="form_button load_visitor_list" style="margin-top: 20px;">Загрузить список</div>
        <?php } ?>
        <div class="table_out">
            <table class="table">
                <thead>
                    <tr>
                        <th>Назначено</th>
                        <th>Компания</th>
                        <th>Администратор</th>
                        <th>Посетитель</th>
                        <th>Добавлен</th>
                        <th>Вошел</th>
                        <th>Вышел</th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
        <div class="iload"></div>
    </div>
    <div class="window mini" rel="visitor_list">
        <div class="inner_content">
            <div class="close"><div class="iclose"></div></div>
            <div class="icontent">
                <h2>Загрузка списка</h2>
                <div class="card_form">
                    <div class="form_item">
                        <label class="form_control">
                            <input type="radio" name="type_date" checked="checked" value="0"/>
                            Дата
                        </label>
                        <input class="form_control datepicker" value="<?=date('d.m.Y', time()+3600*24)?>" style="width:120px;" type="text" name="date"/>
                    </div>
                    <div class="form_item">
                        <label class="form_control">
                            <input type="radio" name="type_date" value="1"/>
                            Период
                        </label>
                        <input class="form_control datepicker"
                               value="<?= date('d.m.Y', time()+3600*24) ?>" style="width:120px;"
                               type="text" name="date_from"/> -
                        <input class="form_control datepicker"
                               value="<?= date('d.m.Y', time()+3600*48) ?>" style="width:120px;"
                               type="text" name="date_to"/>
                    </div>
                    <div class="form_control">
                        <input type="file" name="list"/>
                    </div>
                </div>
                <div class="error"></div>
                <div class="form_button block load_list">Загрузить</div>
            </div>
        </div>
    </div>
<?php } ?>