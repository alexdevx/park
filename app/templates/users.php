<?php if ($view['user']) { ?>
    <h1 class="page_title"><a href="?">Пользователи</a> > <?=$view['title']?></h1>
    <div class="UsersAdmin_Edit">
        <input type="hidden" name="user_id" value="<?=$view['user']->id?>"/>
        <div class="card_form">
            <div class="form_item">
                <label class="form_control">Фотография</label>
                <div class="image4crop" data-name="image" data-image="<?=($view['user']->data['image0'] ? $view['user']->image_config[0]['url'].$view['user']->data['image0'] : '')?>"></div>
            </div>
            <div class="form_item">
                <label class="form_control">Роль</label>
                <select class="form_control" name="type">
                    <?php foreach ($user->types as $itype=>$type) {
                        if (($itype==$view['user']->data['type'])||(!in_array($itype, [0,3]))) {
                            ?>
                            <option <?= ($view['user']->data['type'] == $itype ? 'selected="selected"' : '') ?>
                                    value="<?= $itype ?>"><?= $type ?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </div>
            <div class="form_item user2user">
                <label class="form_control">Доступ в компаниях</label>
                <select class="form_control" multiple="multiple" name="user2user[]" data-placeholder="Выбрать компанию" data-params='{"type":1}'>
                    <?php foreach ($view['user2user']->items as $u) { ?>
                        <option selected="selected" value="<?=$u['user_id']?>"><?=$u['name']?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="form_item">
                <label class="form_control">Имя</label>
                <input class="form_control" value="<?=$view['user']->data['name']?>" type="text" name="name"/>
            </div>
            <div class="form_item">
                <label class="form_control">Телефон</label>
                <input class="form_control" value="<?=$view['user']->data['phone']?>" type="text" name="phone"/>
            </div>
            <div class="form_item">
                <label class="form_control">
                    <input class="form_control" <?=($view['user']->data['active'] ? 'checked="checked"' : '')?> type="checkbox" name="active" value="1"/>
                    Открыт доступ
                </label>
            </div>
            <div class="form_item">
                <label class="form_control">
                    <input class="form_control" <?=($view['user']->data['show_car'] ? 'checked="checked"' : '')?> type="checkbox" name="show_car" value="1"/>
                    Открыт доступ к авто
                </label>
            </div>
            <div class="form_item">
                <label class="form_control">Логин</label>
                <input class="form_control" value="<?=$view['user']->data['email']?>" type="text" name="email"/>
            </div>
            <div class="form_item">
                <label class="form_control"><?=(($view['user']->id) ? 'Новый пароль' : 'Пароль')?></label>
                <input class="form_control" value="" type="password" name="pass"/>
            </div>
        </div>
        <?php if ((!$view['user']->id)||($view['user']->data['type']!=0)) { ?>
            <div class="error"></div>
            <?php if ($view['user']->id) { ?>
                <div class="form_button _cancel_form red delete">Удалить</div>
            <?php } ?>
        <?php } ?>
        <div class="form_button _save_form save">Сохранить</div>
        <div style="clear:both;"></div>
    </div>
<?php } else { ?>
    <h1 class="page_title">Пользователи</h1>
    <div class="LineItems UsersAdmin">
        <div class="items_finder">
            <div class="item_finder text_search">
                <input value="<?=cstr($_GET['text'])?>" class="text" placeholder="Поиск пользователей" type="text"/>
                <div class="find_btn"><i class="fa fa-search" aria-hidden="true"></i></div>
            </div>
            <div class="item_finder not_grow">
                <select name="type">
                    <option value="-1">Роль</option>
                    <?php foreach ($user->types as $i=>$v) { if (!in_array($i, [3])) { ?>
                        <option <?=($i==1 ? 'selected="selected"' : '')?> value="<?=$i?>"><?=$v?></option>
                    <?php } } ?>
                </select>
            </div>
            <div class="item_finder not_grow">
                <select style="min-width:250px;" name="parent_id" data-placeholder="Сотрудники компании" data-params='{"type":1}'">

                </select>
            </div>
        </div>
        <div class="items"></div>
        <div class="iload"></div>
    </div>
<?php } ?>